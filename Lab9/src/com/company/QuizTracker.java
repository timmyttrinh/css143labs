/**
 * QuizTracker class
 *
 * @author Timmy Trinh
 * @version (CSSSKL 143)
 */
package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class QuizTracker {
    ArrayList<QuizScore> tracker = new ArrayList<QuizScore>();

    /**
     * add method
     *
     * @pre none
     * @post add new quiz score to arraylist of quiz scores
     */
    public void add(QuizScore newQ) {
        if (!this.tracker.isEmpty()) {
            ArrayList<QuizScore> temp = new ArrayList<QuizScore>();
            for (int i = 0; i < this.tracker.size(); i++) {
                temp.add(this.tracker.get(i).clone());
            }
            temp.add(newQ.clone());
            this.tracker = temp;
        } else {
            this.tracker.add(newQ.clone());
        }

    }

    /**
     * toString method
     *
     * @pre none
     * @post return string of quiz scores
     */
    @Override
    public String toString() {
        String retVal = "";
        for (int i = 0; i < this.tracker.size(); i++) {
            retVal += this.tracker.get(i).getScore() + ", ";
        }
        return "scores: " + retVal;
    }

    public static void main(String[] args) {
        QuizTracker q = new QuizTracker();
        q.add(new QuizScore(100));
        q.add(new QuizScore(50));
        System.out.println(q);
    }


}
