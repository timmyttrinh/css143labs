/**
 * QuizScore class
 *
 * @author Timmy Trinh
 * @version (CSSSKL 143)
 */
package com.company;

public class QuizScore implements Cloneable {
    private int score;

    /**
     * default QuizScore constructor
     *
     * @pre none
     * @post default value assigned 100
     */
    public QuizScore() {
        this.score = 100;
    }

    /**
     * QuizScore constructor
     *
     * @pre none
     * @post assign score value s
     */
    public QuizScore(int s) {
        this.score = s;
    }

    public QuizScore(QuizScore other) {
        this.score = other.score;
    }

    /**
     * score getter
     *
     * @pre none
     * @post return score
     */
    public int getScore() {
        return score;
    }

    /**
     * score setter
     *
     * @pre none
     * @post assign score value s
     */
    public void setScore(int s) {
        this.score = s;
    }

    /**
     * clone method override
     *
     * @return
     * @pre none
     * @post return deep copy of student object
     */
    @Override
    public QuizScore clone() {
        return new QuizScore(this);
    }

    /**
     * toString method
     *
     * @pre none
     * @post return string of quiz scores
     */
    @Override
    public String toString() {
        return this.score + "";
    }
}
