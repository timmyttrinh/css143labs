/**
 * Student class
 *
 * @author Timmy Trinh
 * @version (CSSSKL 143)
 */
package com.company;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class Student implements Serializable, Comparable, Cloneable {
    private double GPA;
    private String name;

    public Student(String n, double gpa) {
        name = n;
        GPA = gpa;
    }

    /**
     * copy constructor
     *
     * @pre none
     * @post creates a deep copying using other data members
     */
    public Student(Student other) {
        this.name = other.name;
        this.GPA = other.GPA;
    }

    /**
     * clone method override
     *
     * @return
     * @pre none
     * @post return deep copy of student object
     */
    @Override
    public Student clone() {
        return new Student(this);
    }

    /**
     * compareTo method
     *
     * @pre none
     * @post return this GPA subtracted by other GPA
     */
    public double compareTo(Student other) {
        return this.GPA - other.GPA;
    }

    /**
     * toString method Override
     *
     * @pre none
     * @post return the name and GPA of the student
     */
    @Override
    public String toString() {
        return this.name + " | GPA: " + GPA;
    }

    /**
     * GPA getter
     *
     * @pre none
     * @post return GPA
     */
    public double getGPA() {
        return GPA;
    }

    /**
     * GPA setter
     *
     * @pre none
     * @post set GPA to value newGPA
     */
    public void setGPA(double newGPA) {
        this.GPA = newGPA;
    }

    /**
     * name getter
     *
     * @pre none
     * @post return name
     */
    public String getName() {
        return name;
    }

    /**
     * name setter
     *
     * @pre none
     * @post set name to value newName
     */
    public void setName(String newName) {
        this.name = newName;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        writeObjectToFile();
        Student a = readObjectFromFile();

        if (a != null) {
            System.out.println(a.toString());
        }

        Student s1 = new Student("s1", 4.0);
        Student s2 = new Student("s2", 1.0);
        Student s3 = new Student("s3", 2.75);
        Student s4 = new Student("s4", 2.0);
        Student s5 = new Student("s5", 3.2);
        Student s6 = new Student("s6", 3.0);
        Student s7 = new Student("s7", 3.0);
        Student s8 = new Student("s8", 1.5);
        Student s9 = new Student("s9", 4.0);
        Student s10 = new Student("s10", 4.0);
        System.out.println(s1 + " compared to " + s2 + " | " + s1.compareTo(s2));
        System.out.println(s3 + " compared to " + s4 + " | " + s3.compareTo(s4));
        System.out.println(s5 + " compared to " + s6 + " | " + s5.compareTo(s6));
        System.out.println(s7 + " compared to " + s8 + " | " + s7.compareTo(s8));
        System.out.println(s9 + " compared to " + s10 + " | " + s9.compareTo(s10));

    }

    private static Student readObjectFromFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream("data.obj"));
        Student one = (Student) is.readObject();

        is.close();

        return one;
    }

    private static void writeObjectToFile() throws FileNotFoundException, IOException {
        Student nguyen = new Student("Nguyen", 3.5);

        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("data.obj"));
        os.writeObject(nguyen);

        os.close();
    }

    @Override
    public int compareTo(Object o) {
        return -1;
    }
}
