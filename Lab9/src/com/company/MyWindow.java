/**
 * MyWindow class
 *
 * @author Timmy Trinh
 * @version (CSSSKL 143)
 */
package com.company;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.JFrame;

public class MyWindow extends JFrame implements MouseListener {
    ArrayList myShapes = new ArrayList();

    public MyWindow() {
        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        addMouseListener(this);
    }

    /**
     * mouseClicked method
     *
     * @pre none
     * @post everytime the mouse is clicked, a student is added to the arraylist,
     * with a gpa based on the x and y
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("added a student to arraylist");
        double x = e.getX() * .01;
        double y = e.getY() * .01;
        double GPA = x + y;
        this.myShapes.add(new Student("a student", GPA));
        System.out.println(myShapes);
    }

    /**
     * mousePressed
     *
     * @pre none
     * @post print the x and y of the mouse press
     */
    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("you pressed the mouse on " + e.getX() + "," + e.getY());
    }

    /**
     * mouseReleased
     *
     * @param e
     * @pre none
     * @post print the x and y of the mouse release
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("you released the mouse on " + e.getX() + "," + e.getY());
    }

    /**
     * mouseEntered method
     *
     * @pre none
     * @post print "the mouse has entered the window" when
     * the cursor is on the window
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("the mouse has entered the window");
    }

    /**
     * mouseExited method
     *
     * @pre none
     * @post print "the mouse has left the window" when
     * the cursor is not on the window
     */
    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("the mouse has left the window");
    }

    public static void main(String[] args) {
        JFrame app = new MyWindow();
    }
//todo: add MouseListener methods (see outline below)
}