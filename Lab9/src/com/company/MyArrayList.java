package com.company;

import java.util.*;

/**
 * This class implements multiple sort algorithms to be used with ints, chars, and Stings.
 * Bubble sort, Selection sort, and Insertion sorts are implemented here
 *
 * @author Timmy Trinh
 * @version (CSSSKL 143)
 */
// the Big O of the improved selection sort is O(n^2) where the normal
// selection sort is also O(n^2)
public class MyArrayList implements Comparable {

    // instance data
    protected int[] IntList;
    protected char[] CharList;
    protected String[] StringList;

    // constructor will build all 3 arrays here
    public MyArrayList() {
        this.IntList = new int[10];
        this.CharList = new char[10];
        this.StringList = new String[5];

        // fill all 3 arrays with data
        for (int i = 0; i < IntList.length; i++) {
            IntList[i] = (int) (Math.random() * 52);
        }

        // Populate char array
        for (int i = 0; i < CharList.length; i++) {

            Random random = new Random();
            CharList[i] = (char) (random.nextInt(26) + 'a');
        }

        // Populate String array
        StringList[0] = "joe";
        StringList[1] = "mark";
        StringList[2] = "abbey";
        StringList[3] = "tony";
        StringList[4] = "kevin";
    }

    /**
     * compareTo method
     *
     * @pre none
     * @post return -1 if this arrayList is less than other, 1 if greater, 0 if equal
     */
    public int compareTo(MyArrayList other) {
        // your code here
//        System.out.println("compareTo() is returning -1, array[0] < other[0]");


        // your code here
//        System.out.println("compareTo() is returning 1, array[0] > other[0]");


//        System.out.println("compareTo() is returning 0, array[0] != other[0] ");
        // return a value here

        if (this.IntList[0] < other.IntList[0]) return -1;
            // -1 is returned when the first element in this array is less than the other
            // first element in the array
        else if (this.IntList[0] > other.IntList[0]) return 1;
            // 1 is returned when the first element in this array is greater than the other
            // first element in the array
        else return 0;
        // 0 is returned if this first element is equal to the other first element

    }

    /**
     * intBubbleSort method
     *
     * @pre none
     * @post sort an array of ints using bubble sort
     */
    public void intBubbleSort() {
        // Implement your sort, call a helper swap method
        boolean stillSwapping = true;
        while (stillSwapping) {
            stillSwapping = false;
            for (int i = 0; i < this.IntList.length - 1; i++) {
                if (this.IntList[i] > this.IntList[i + 1]) {
                    swapInts(this.IntList, i);
                    stillSwapping = true;
                }
            }
        }
    }

    /**
     * CharBubbleSort method
     *
     * @pre none
     * @post sort an array of chars using bubble sort
     */
    public void CharBubbleSort() {
        // Implement your sort, call a helper swap method
        boolean stillSwapping = true;
        while (stillSwapping) {
            stillSwapping = false;
            for (int i = 0; i < this.CharList.length - 1; i++) {
                if ((int) this.CharList[i] > (int) this.CharList[i + 1]) {
                    swapChars(this.CharList, i);
                    stillSwapping = true;
                }
            }
        }
    }

    /**
     * stringBubbleSort method
     *
     * @pre none
     * @post sort an array of strings using bubble sort
     */
    public void stringBubbleSort() {
        // Implement your sort, call a helper swap method
        boolean stillSwapping = true;
        while (stillSwapping) {
            stillSwapping = false;
            for (int i = 0; i < this.StringList.length - 1; i++) {
                if (this.StringList[i].compareTo(this.StringList[i + 1]) > 0) {
                    swapStrings(this.StringList, i);
                    stillSwapping = true;
                }
            }
        }
    }

    /**
     * swapInts method
     *
     * @pre none
     * @post intBubbleSort method helper, swap ints in an array
     */
    public void swapInts(int[] intList, int j) {
        // code for swapping ints
        int temp = intList[j];
        intList[j] = intList[j + 1];
        intList[j + 1] = temp;
    }

    /**
     * swapChars method
     *
     * @pre none
     * @post CharBubbleSort method helper, swap chars in an array
     */
    public void swapChars(char[] charList, int j) {
        // code for swapping chars
        char temp = charList[j];
        charList[j] = charList[j + 1];
        charList[j + 1] = temp;
    }

    /**
     * swapStrings method
     *
     * @pre none
     * @post stringBubbleSort method helper, swap strings in an array
     */
    public void swapStrings(String[] stringList, int j) {
        // code for swapping Strings
        String temp = stringList[j];
        stringList[j] = stringList[j + 1];
        stringList[j + 1] = temp;
    }

    /**
     * selectionSort method
     *
     * @pre none
     * @post use selection sort to sort an array of ints
     */
    //selection sort for ints
    public void selectionSort() {
        int minIndex;
        for (int i = 0; i < this.IntList.length - 1; i++) {
            minIndex = findSmallest(this.IntList, i, this.IntList.length);
            swapSelection(this.IntList, i, minIndex);
        }

    }

    /**
     * improvedSelectionSort
     *
     * @pre none
     * @post selection sort using max and min
     */
    public void improvedSelectionSort() {
        for (int i = 0, j = this.IntList.length - 1; i < j; i++, j--) {
            int min = this.IntList[i];
            int max = this.IntList[i];
            int minIndex = i;
            int maxIndex = i;

            for (int k = i; k <= j; k++) {
                if (this.IntList[k] > max) {
                    max = this.IntList[k];
                    maxIndex = k;
                } else if (this.IntList[k] < min) {
                    min = this.IntList[k];
                    minIndex = k;
                }
            }

            swapSelection(this.IntList, i, minIndex);

            if (this.IntList[minIndex] == max) {
                swapSelection(this.IntList, j, minIndex);
            } else {
                swapSelection(this.IntList, j, maxIndex);
            }
        }
    }

    //selection sort for Strings
    public void stringSelectionSort() {
        // Implement your sort, call swapSelectionStrings(String[] StringList, int i)
        // and findSmallest(IntList, i, IntList.length) from your method

    }

    /**
     * swapSelection method
     *
     * @pre none
     * @post helper method for selection sort method, swaps elements in an array
     */
    public void swapSelection(int[] intList, int i, int nextMin) {
        // Your code here to swap int values
        int temp = intList[nextMin];
        intList[nextMin] = intList[i];
        intList[i] = temp;
    }

    public void swapSelectionStrings(String[] StringList, int i) {
        // Your code here to swap values

    }

    /**
     * findSmallest method
     *
     * @pre none
     * @post functions as a helper method for selection sort, returning index of the
     * smallest element
     */
    public int findSmallest(int[] arr, int begin, int end) {
        //returns the index of the smallest element
        int minIndex = begin;       //hint
        for (int i = begin + 1; i < end; i++) {
            if (arr[i] < arr[minIndex]) {
                minIndex = i;
            }
        }
        return minIndex;
    }

    /**
     * findLargest method
     *
     * @pre none
     * @post functions as a helper method for selection sort, returning index of the
     * largest element
     */
    public int findLargest(int[] arr, int begin, int end) {
        int maxIndex = begin;       //hint
        for (int i = begin + 1; i < end; i++) {
            if (arr[i] > arr[maxIndex]) {
                maxIndex = i;
            }
        }
        return maxIndex;

    }

    /**
     * insertion sort method
     *
     * @pre none
     * @post using insertion sort to sort and array of ints
     */
    //Insertion Sort
    public void insertionSort() {

        for (int i = 1; i < this.IntList.length; i++) {
            //note -1 above since we’re dealing with neighbors (a, a+1)
            int current = this.IntList[i];
            int hole = i - 1;

            while (hole > -1 && this.IntList[hole] > current) { //while “out of place”
                //slide data to the left moving the “hole” left
                // .
                //  .  // more code goes here
                // .
                // .
                this.IntList[hole + 1] = this.IntList[hole];
                hole--;

            }
            this.IntList[hole + 1] = current;
        }
    }

    /**
     * not used
     */
    @Override
    public int compareTo(Object o) {
        return 0;
    }
}





