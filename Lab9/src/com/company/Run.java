/**
 * Run class
 *
 * @author Timmy Trinh
 * @version (CSSSKL 143)
 */
package com.company;

public class Run implements Runnable {
    String name;

    /**
     * default constructor
     *
     * @pre none
     * @post assign name "default"
     */
    public Run() {
        this.name = "default";
    }

    /**
     * Run constructor
     *
     * @pre none
     * @post assign name value t
     */
    public Run(String t) {
        this.name = t;
    }

    /**
     * run method, override
     *
     * @pre none
     * @post generates random numbers
     */
    @Override
    public void run() {
        for (int i = 0; i < 500; i++) {
            if (Math.random() < .5) {   //on occasion
                try {    //lets go to sleep so the other threads have a chance
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    //do nothing other than a trace
                    e.printStackTrace();
                }
            }
            System.out.println(name + " : " + i);
        }
    }

    /**
     * print method
     *
     * @pre none
     * @post print the name of the runnable class
     */
    public void print() {
        System.out.println(this.name);
    }

    public static void main(String[] args) {
        Thread t1 = new Thread( new Run("foo1") );
        Thread t2 = new Thread( new Run("foo2") );

        t1.start();	//this calls run()
        t2.start();	//start the second thread

        Run r = new Run("blank");
        r.print();
    }
}
