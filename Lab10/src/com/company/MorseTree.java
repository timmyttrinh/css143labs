/**
 * MorseTree
 * CSSSKL 143
 *
 * @author Timmy Trinh borrowed from Rob Nash
 * @version 03/15/2021
 * /**
 */
package com.company;

import com.sun.source.tree.Tree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;;
/*
 * MorseTree.java
 * CSSSKL 143 Binary Search Tree Lab
 * Author: Rob Nash
 *
 * This class reads in data from a text file ("data.txt") and populates a binary tree with an
 * ordering constraint.  See the lab instructions for more information, but in general, dots go right
 * and dashes go left when constructing or traversing a Morse code tree.  Search for //TODO
 * in the code to see what code you have to implement.
 *
 * Start with the constructor. In your constructor read each line in from the textfile first,
 * calling add() for each {letter, morseCodeStr} pair.
 *
 */

public class MorseTree {
    //TODO: data member called "root" goes here
    private TreeNode<Character> root;

    //TODO: Complete constructor

    /**
     * MorseTree constructor
     *
     * PRE: none
     * POST: uses a while loop to load data from a txt file onto a binary tree
     * if no file found "no file found" is printed
     */
    public MorseTree() {

        //first, open data.txt, add each line to the tree
        Scanner fin;
        try {
            //for each line in the file,
            //  get the letter(char) and the Morse string
            //  call add() with this data
            //  print out the letter and Morse string here for debugging
        fin = new Scanner(new File("data.txt"));
            while(fin.hasNextLine()){
                String data = fin.nextLine();
                add(data.substring(2), data.charAt(0));
            }

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            System.out.println("no file found");
            e.printStackTrace();
        }

    }

    /**
     * add method
     *
     * PRE: none
     * POST: call insertInSubtree to add to binary tree
     */
    public void add(String morseStr, char letter) {
        root = insertInSubtree(morseStr, letter, root);
    }

    // add helper
    //TODO: recursively complete this function.  It's only a few characters different from findInSubtree()
    private TreeNode<Character> insertInSubtree(String morseStr, char letter, TreeNode<Character> subtree) {
        //base case 1 : subtree is null
        if(subtree == null){
            return insertInSubtree(morseStr, letter, new TreeNode<Character>());
        }
        //base case 2 : morseStr is of length 0
        if(morseStr.length() == 1){
            subtree.data = letter;
            return subtree;
        } else if(morseStr.charAt(0) == '.') {
            subtree.setRight(insertInSubtree(morseStr.substring(1), letter, subtree.right));
        } else if(morseStr.charAt(0) == '-'){
            subtree.setLeft(insertInSubtree(morseStr.substring(1), letter, subtree.left));
        }
        //recursive case 1: the first char in morseStr is a '.', so recursively traverse tree
        //recursive case 2: the first char in the morseStr is a '-', so recurse accordingly
        return subtree;  //always the last line, always return the node you are working on
    }

    /**
     * translate method
     *
     * PRE: none
     * POST: translates morse code to letters
     */
    public Character translate(String morseStr) {
        return findInSubtree(morseStr, root);
    }

    //translate helper
    //TODO: recursively complete this function.  Very similar to insertInSubtree()
    private Character findInSubtree(String morseStr, TreeNode<Character> subtree) {
        //base case 1 : subtree is null
        if(subtree == null){
            return null;
        }
        if(morseStr.length() == 0){
            return (Character) subtree.data;
        } else if(morseStr.charAt(0) == '.'){
            return findInSubtree(morseStr.substring(1), subtree.right);
        } else if(morseStr.charAt(0) == '-'){
            return findInSubtree(morseStr.substring(1), subtree.left);
        }
        //base case 2 : morseStr is of length 0
        //recursive case 1: the first char in morseStr is a '.', so recursively traverse tree
        //recursive case 2: the first char in the morseStr is a '-', so re-curse accordingly
        return null;  //remove this
    }

    /**
     * translateString method
     *
     * PRE: none
     * POST: translates a a combination of morse code into a String
     */
    //TODO: Non-recursive function that calls other (recursive) functions
    public String translateString(String tokens) {
        String retVal = "";
        //build a scanner here using tokens as input
        //iterate over the tokens calling translate on each token (substring separated by a space)
        //concat these characters and return them
        Scanner fin = new Scanner(tokens);
        while(fin.hasNext()){
            retVal += findInSubtree(fin.next(), this.root);
        }
        fin.close();
        return retVal;
    }

    public String toMorseCode(Character c) {
        //walk the tree looking for the TreeNode with the char c in it
        //preorder walk?
        //inorder walk?
        //postorder walk?

        //when you've found the char c, report the path from the root to the node
        //and build the morse code by adding a "." when you go right, "-" when you go left
        return new String("You wish.");
    }

    public String toString() {
        return inorderWalk();
    }

    private String inorderWalk() {

        return new String("Another wish.");
    }

    public static void main(String[] args) {
        MorseTree mt = new MorseTree();  //builds our tree using data from a file

        System.out.println(mt.translate("..."));  //prints out S
        System.out.println(mt.translate("---"));  //prints out O
        System.out.println(mt.translate(".......-"));  //prints out null

        System.out.println(mt.translateString("... --- ..."));  //SOS
        System.out.println(mt.toMorseCode('S'));  //find where we are in the tree, remember path to root
    }

    // Inner class to create the linked structure
    private class TreeNode<Character> {

        Object data;     // holds a given node’s data
        TreeNode<Character> right;
        TreeNode<Character> left;

        public TreeNode() {
            this.data = null;
            this.right = null;
            this.left = null;
        }

        public TreeNode(TreeNode<Character> left, TreeNode<Character> right, Object data){
            this.data = data;
            this.left = left;
            this.right = right;
        }
        public void setRight(TreeNode<Character> rightNode) {
            this.right = rightNode;
        }

        public void setLeft(TreeNode<Character> leftNode) {
            this.left = leftNode;
        }

    }
}

