package com.company;

import com.sun.source.tree.Tree;

import java.io.*;
import java.util.*;

/**
 * Class invariant: This code for a binary tree satisfies the
 * binary search tree storage rule.
 * CSSSKL 143
 *
 * @author Timmy Trinh
 * @version 03/15/2021
 * /**
 */

public class CharTree {

    /*Inner class Node, 2 references(pointers), one data element
     * The only reason this inner class is static is that it is used in
     * the static methods insertInSubtree , isInSubtree , and
     * showElementsInSubtree. This class should have more methods.
     * This is just a sample of possible methods.
     */
    private static class TreeNode {

        // Declare private data type char
        // Declare 2 links, rightLink & leftLink of type TreeNode
        private char data;
        TreeNode rightLink;
        TreeNode leftLink;

        // Parametrized constructor to build a node
        public TreeNode(char newData, TreeNode newLeftLink, TreeNode newRightLink) {
            // complete the constructor
            this.data = newData;
            this.rightLink = newRightLink;
            this.leftLink = newLeftLink;
        }
    }           //End of IntTreeNode inner class

    // The first node of the tree, called root
    private TreeNode root;

    // Default constructor to build the CharTree
    public CharTree() {
        root = null;
    }

    // Utility methods for CharTree:
    public void add(char item) {
        root = insertInSubtree(item, root);
    }

    public boolean contains(char item) {
        return isInSubtree(item, root);
    }

    public void showElements() {
        showElementsInSubtree(root);
    }

    /**
     * Returns the root node of a tree that is the tree with root node
     * subTreeRoot, but with a new node added that contains item.
     */
    private static TreeNode insertInSubtree(char item, TreeNode subTreeRoot) {
        if (subTreeRoot == null)
            return new TreeNode(item, null, null);
        else if (item < subTreeRoot.data) {
            subTreeRoot.leftLink = insertInSubtree(item, subTreeRoot.leftLink);
            return subTreeRoot;
        } else {         //item >= subTreeRoot.data
            subTreeRoot.rightLink = insertInSubtree(item, subTreeRoot.rightLink);
            return subTreeRoot;
        }
    }

    /**
     * Returns true if item is found in the subTree, and false if not found.
     */
    private static boolean isInSubtree(char item, TreeNode subTreeRoot) {
        // base case: is subTreeRoot null?    then return false
        if (subTreeRoot == null) {
            return false;
        } else if (subTreeRoot.data == item) {
            return true;
        } else if (item < subTreeRoot.data) {
            return isInSubtree(item, subTreeRoot.leftLink);
        } else if (item >= subTreeRoot.data) {
            return isInSubtree(item, subTreeRoot.rightLink);

        }
        // else if subTreeRoot.data == item   what would you return?

        // else item < subTreeRoot.data
        // recursive call

        //else         // item >= link.data
        // recursive call

        //return stub (remove it)
        return false;
    }

    /**
     * toString override
     *
     * PRE: none
     * POST: return string of tree
     */
    @Override
    public String toString() {
        return toString(root);
    }
//helper
    private static String toString(TreeNode root) {
        if (root == null) {
            return "";
        } else {
            return toString(root.leftLink) + " " + root.data + " " + toString(root.rightLink);
        }
    }

    /**
     * countNodes method
     *
     * PRE: none
     * POST: count nodes in tree
     */
    public int countNodes() {
        return countNodes(this.root);
    }
//helper
    private int countNodes(TreeNode n) {
        if (n == null) {
            return 0;
        } else {
            return countNodes(n.leftLink) + countNodes(n.rightLink) + 1;
        }
    }

    /**
     * getDepth method
     *
     * PRE: none
     * POST: return depth of tree
     */
    public int getDepth() {
        return getDepth(this.root);
    }
//helper
    private int getDepth(TreeNode root) {
        if (root != null) {
            return Math.max(getDepth(root.leftLink), getDepth(root.rightLink) + 1);
        } else {
            return 0;
        }
    }

    /**
     * getParent method
     *
     * PRE: none
     * POST: return char of parent node
     */
    public char getParent(char child) {
        TreeNode parent = getParent(child, this.root, null);
        if(parent != null){
            return parent.data;
        } else {
            System.out.println("does not have parent");
            return (char) 0;

        }
    }
//helper
    private TreeNode getParent(char child, TreeNode root, TreeNode parent) {
        if (root == null) {
            return null;
        } else if (root.data != child) {
            parent = getParent(child, root.leftLink, root);
            if (parent == null) {
                parent = getParent(child, root.rightLink, root);
            }
        }
        return parent;
    }

    /**
     * Print all tree node's data using inorder traversal.
     */
    private static void showElementsInSubtree(TreeNode subTreeRoot) {
        if (subTreeRoot != null) {
            showElementsInSubtree(subTreeRoot.leftLink);
            System.out.print(subTreeRoot.data + " ");
            showElementsInSubtree(subTreeRoot.rightLink);
        }                    //else do nothing. Empty tree has nothing to display.
    }

    public static void main(String[] args) {
        CharTree tree = new CharTree();
        tree.add('c');
        tree.add('a');
        tree.add('t');
        tree.add('s');
        showElementsInSubtree(tree.root);

        System.out.println("Testing contains:");
        System.out.println("s is found: " + tree.contains('s'));
        System.out.println("b is found: " + tree.contains('b'));

        //Uncomment the remaining methods as you add them to the class.
        //Remember to have these public methods call a private helper method
        //to actually implement traversing the tree recursively.

        System.out.println(tree.toString());   //inorder prints a c t
        System.out.println("Number of nodes in tree: " + tree.countNodes());
        System.out.println("Tree depth: " + tree.getDepth());

        System.out.println("Parent of a: " + tree.getParent('a'));  //can be tricky
        System.out.println("Parent of s: " + tree.getParent('s'));
        System.out.println("Parent of b: " + tree.getParent('b'));

//        Uncomment if doing the extra credit
//        removeDriver();

    }

    public static void removeDriver() {
        CharTree tree = new CharTree();
        tree.add('5');
        tree.add('3');
        tree.add('7');
        tree.add('1');
        tree.add('2');
        tree.add('4');
        tree.add('6');
        tree.add('8');
        tree.showElements();

        //Test case 1: remove item not there
        //System.out.println("remove 9:" + tree.remove('9')); //false
        //tree.showElements(); //remains unchanged

        //Test case 2: remove a leaf
        //System.out.println("remove 4:" + tree.remove('4')); //true
        //tree.showElements(); // 1 2 3 5 6 7 8

        //Test case 3: remove node with 1 child
        //System.out.println("remove 1:" + tree.remove('1')); //true
        //tree.showElements(); // 2 3 5 6 7 8

        //Test case 4: remove node with 2 children
        //System.out.println("remove 7:" + tree.remove('7')); //true
        //tree.showElements(); // 2 3 5 6 8

        //Test case 5: remove root
        //System.out.println("remove 5:" + tree.remove('5')); //true
        //tree.showElements(); // 2 3 6 8
    }
}
