/**
 * @author Timmy Trinh
 * @since 02/17/2021
 * FractalFlake class, inherits from Shape
 */
package com.company;

import java.awt.*;

public class FractalFlake extends Shape {
    private final int limit;
    private final int branch;
    private int size;

    /**
     * FractalFlake constructor
     *
     * @pre none
     * @post call super class using args a and b, assign branch value 7,
     * assign limit value 25
     */
    public FractalFlake(int a, int b) {
        super(a, b);
        this.branch = 12;// default value
        this.limit = 25; // default value
        this.size = 25;
    }


    /**
     * FractalFlake constructor
     *
     * @pre none
     * @post call super class using args x and y, assign size value s,
     * assign branch value b, assign limit value l
     */
    public FractalFlake(int x, int y, int s, int b, int l) {
        super(x, y);
        if (l < 1 || l > 50) {
            l = 50;
        }
        if (b < 5 || b > 12) {
            b = 12;
        }
        this.size = s;
        this.branch = b;
        this.limit = l;
    }

    /**
     * draw Override facade
     *
     * @pre none
     * @post calls draw
     */
    @Override
    public void draw(Graphics g) {   //a redirect or facade
        draw(g, getX(), getY(), limit);
    }

    /**
     * private draw method
     *
     * @pre none
     * @post gets called in draw facade
     */
    private void draw(Graphics g, int startX, int startY, int limit) {
        if (limit >= 3) {
            for (int i = 0; i < branch; i++) {
                int x2 = startX + (int) (size * Math.cos((2 * Math.PI / branch) * i));
                int y2 = startY - (int) (size * Math.sin((2 * Math.PI / branch) * i));

                g.drawLine(startX, startY, x2, y2);
                draw(g, x2, y2, limit / 3);
            }
        }
    }

    /**
     * limit getter
     *
     * @pre none
     * @post return limit
     */
    public int getLimit() {
        return this.limit;
    }

    /**
     * branch getter
     *
     * @pre none
     * @post return branch
     */
    public int getBranch() {
        return this.branch;
    }

    /**
     * size getter
     *
     * @pre none
     * @post return size
     */
    public int getSize() {
        return this.size;
    }

    /**
     * size setter
     *
     * @pre size cannot be negative
     * @post assign size value s
     */
    public void setSize(int s) {
        this.size = s;
    }
}
