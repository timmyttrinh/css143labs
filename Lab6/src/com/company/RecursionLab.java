/**
 * @author Timmy Trinh
 * @since 02/17/2021
 * RecursionLab
 */
package com.company;

/*-----------------------------------------------------------------------------------
 *
 *	sum( n ) is a summation algorithm defined as follows:
 *
 *	(1)		sum( n ) =  n + (n-1) + (n-2) + ... + 1
 * 	(1a) 	sum( 1 ) = 1
 *
 * and from this definition, we can rewrite this formula in terms of itself, such that:
 *
 *	(2)	    sum( n ) = n + sum( n - 1 )
 *
 * and we can do this again
 *
 *	(3)    	sum( n ) = n + ( n - 1) + sum( n - 2 )
 *
 * and so on, and so forth, we finally end up with the same as above
 *
 *	(1)	    sum( n ) = n + (n-1) + (n-2) + ... + 1
 *
 *----------------------------------------------------------------------------------- */

import java.awt.Dimension;

import javax.swing.*;

public class RecursionLab {

    private static JTextArea myArea = new JTextArea();
    private static int count = 0;

    public static void main(String args[]) {    //invoke the recursive method here...

        /**
         * TODO: switch between the two commented lines below and execute this code,
         * observing the output for both the iterative solution and the recursive solution.
         * To watch the recursive behaviour in action, set a breakpoint on the if statement
         * inside the recursiveSum() function
         *
         */
//        int solution = iterativeSum( 20 );
//        int solution = recursiveSum( 20 );
//
//
//        //Some GUI details
//        myArea.setText(("Result is : " + solution + "\n" + myArea.getText()));
//        JScrollPane myPane = new JScrollPane( myArea );
//        myPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
//        myPane.setPreferredSize(new Dimension(600,300));
//        JOptionPane.showMessageDialog( null, myPane );
//
//        //good form to include an exit call when GUIing in Java
//        System.exit(0);

        // fact tests
        System.out.println(factorial(5));
        // exp tests
        System.out.println(exp(2, 0));
        System.out.println(exp(-2, 3));
        System.out.println(System.nanoTime()); // 384318653490300
        System.out.println(exp2(2, 0));
        System.out.println(exp2(-2, 3));
        System.out.println(System.nanoTime()); // the second method was faster 384318653543200
        // fib tests
        System.out.println(fib(0));
        System.out.println(fib(1));
        System.out.println(fib(5)); // should be 5
        System.out.println(fib(10)); // should be 55
        // c tests
        System.out.println(c(6, 3));
    }

    /**
     * recursive factorial method
     *
     * @pre n cannot be negative
     * @post return factorial of n
     */
    public static int factorial(int n) {
        if (n == 1) {
            return 1; //for summation, factorials, exponent, and Fibonacci, this is 1
        } else {
            //typically the recursive call decrements by 1 or n/2
            return factorial(n - 1) * n;
        }
    }

    /**
     * recursive Exponent method
     *
     * @pre none
     * @post return x to the power of n
     */
    public static int exp(int x, int n) {
        if (n < 0) {
            return -1;
        }
        if (x < 0) {
            x = Math.abs(x);
        }
        if (n == 0) {
            return 1;
        } else {
            return x * exp(x, n - 1);
        }
    }

    /**
     * recursive exponent method, uses different logic
     *
     * @pre none
     * @post return x to the power of n
     */
    public static int exp2(int x, int n) {
        if (n < 0) {
            return -1;
        }
        if (x < 0) {
            x = Math.abs(x);
        }
        if (n == 0) {
            return 1;
        } else if ((n % 2) == 0) {
            return exp2(x, n / 2) * exp2(x, n / 2);
        } else {
            return x * ((exp2(x, (n - 1) / 2)) * (exp2(x, (n - 1) / 2)));
        }
    }

    /**
     * recursive fibonacci method
     *
     * @pre n should noe be negative
     * @post return fibonacci sequence of number n
     */
    public static int fib(int n) {
        if (n == 0)
            return 0;
        else if (n == 1)
            return 1;
        else
            return fib(n - 1) + fib(n - 2);
    }

    /**
     * recursive choose function
     *
     * @pre none
     * @post return n choose r
     *
     */
    public static int c(int n, int r) {
        if (n < r) {
            return 0;
        }
        if (r == 0 || r == n) {
            return 1;
        }
        return c(n - 1, r - 1) + c(n - 1, r);
    }

    /**
     * recursion is similar to iterative looping, but we
     * use method calls to repeat computations (or decompose the problem)
     * instead of explicit looping control structures
     */
    public static int recursiveSum(int n) {
        updateRecursiveDisplay(n);            //overhead for nice output, not required
        if (n == 1)            //if we're at the base case...
            return 1;        //then return the answer to the simplest problem which we know how to solve
        else                //otherwise, we rely on the fact that sum( n ) = n + sum( n - 1 ) and keep recursing
            return (n + recursiveSum(n - 1));
    }                        //for this method to terminate, we must be breaking the problem down into smaller
    //and smaller problems, until we reach the simplest form of the problem which we know
    //how to solve (in this case, it's the fact that sum( 1 ) == 1 )

    //the iterative counterpart to the above recursion
    //notice how it's longer? At times, an iterative solution may require more code than the recursive counterpart,
    //but, the recursive solution is slower and more memory intensive.  We can always recast recursion as iteration.
    public static int iterativeSum(int i) {
        int total = 0;

        for (int n = i; n >= 1; n--) {
            updateIterativeDisplay(n);
            total = total + n;
        }
        return total;
    }

    public static void updateIterativeDisplay(int n) {
        count++;
        String text = myArea.getText();

        text += "\n/*******************Loop iteration " + count + "**************************************";
        text += "\n Calling iterativeSum( int n = " + n + " ). Total += " + n;
        text += "\n***************************************************************************/";

        myArea.setText(text);
    }


    //ignore this method unless interested in the output string
    public static void updateRecursiveDisplay(int n) {

        count++;
        String text = myArea.getText();


        if (count == 1) {
            text += "\n       return ( n + recursiveSum( n - 1 ) ) \n\n";
            text += "       CALL STACK IN MAIN MEMORY                ";
        }


        text += "\n/*******************Method invocation " + count + "*********************";


        text += "\n Calling recursiveSum( int n = " + n + " ). ";
        text += "\n The return statement from this function will resolve in " + (n - 1) + " more recursive method calls...";

        if (n != 1) {
            text += "\n The return statement which invokes the recursive call is \"return ( " + n + " + recursiveSum( " + (n - 1) + " ));";
        } else {
            text += "\n The base case has been hit.  The return statement is \"return 1;\" which is the value returned to the expression above. ";
            text += "\n Notice how hitting the base case will provide a solid, known piece of information from which we will construct more known ";
            text += "\n information by bubbling up through all of the other, yet-to-be-determined return expressions";
        }
        text += "\n***************************************************************************/";

        myArea.setText(text);

    }
}





