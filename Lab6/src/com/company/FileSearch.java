/**
 * @author Timmy Trinh
 * @since 02/17/2021
 * FileSearch class
 */
package com.company;

import java.io.File;
import java.util.Stack;

public class FileSearch {
    public static void main(String[] args) {
        // tests
        System.out.println(iterativeSearchFiles(new File("C:\\Users\\timmy\\IdeaProjects"), "Main.java"));
        System.out.println(recSearchFiles(new File("C:\\Users\\timmy\\IdeaProjects"), "Main.java"));
    }

    /**
     * iterativeSearchFiles method, uses while loop and stack data structure
     *
     * @pre path should exist
     * @post use a stack to return absolute path of target
     */
    public static String iterativeSearchFiles(File path, String target) {
        Stack<File> directories = new Stack<File>();
        directories.push(path);
        while (!directories.isEmpty()) {
            File popped = directories.pop();
            if (popped != null && popped.exists()) {
                if (popped.isDirectory()) {
                    File[] list = popped.listFiles();
                    for (File f : list) {
                        directories.push(f);
                    }
                } else if (popped.isFile()) {
                    if (target.equals(popped.getName())) {
                        return popped.getAbsolutePath();
                    }
                }
            }

        }
        return "not found";
    }

    /**
     * recSearchFiles method, uses recursion
     *
     * @pre path should exist
     * @post use recursion to loop through directories to find target, return target absolute path
     */
    public static String recSearchFiles(File path, String target) {
        File[] list = path.listFiles();
        if (list != null) {
            for (File file : list) {
                if (file.isDirectory()) {
                    recSearchFiles(file, target);
                } else if (target.equals(file.getName())) {
                    System.out.println(file.getAbsolutePath());
                    return file.getAbsolutePath();

                }
            }
        }
        return "";
    }
}
