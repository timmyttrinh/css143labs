/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/15/2021
 * Point2D class
 */
package com.company;

public class Point2D {
    private int x;
    private int y;

    /**
     * Point2D
     * <p>
     * default constructor
     */
    public Point2D() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * Point2D
     * <p>
     * Parameterized constructor
     */
    public Point2D(int newX, int newY) {
        this.x = newX;
        this.y = newY;
    }

    /**
     * setX
     * <p>
     * set x to newX
     */
    public void setX(int newX) {
        this.x = newX;
    }

    public void setY(int newY) {
        this.y = newY;
    }

    /**
     * getX
     * <p>
     * return x
     */
    public int getX() {
        return this.x;
    }

    /**
     * getY
     * <p>
     * return y
     */
    public int getY() {
        return this.y;
    }

    /**
     * resetToOrigin
     * <p>
     * sets x and y to 0
     */
    public void resetToOrigin() {
        setX(0);
        setY(0);
    }

    /**
     * translate
     * <p>
     * adds addX to x and adds addY to y
     */
    public void translate(int addX, int addY) {
        this.x = this.x + addX;
        this.y = this.y + addY;
    }

    /**
     * toString Override
     *
     * return string represent
     */
    @Override
    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }

    /**
     * equals
     *
     * returns true if this x and y are equal to that x and y
     */
    public boolean equals(Point2D that){
        return this.x == that.x && this.y == that.y;
    }

    public static void main(String[] args) {
        Point2D a = new Point2D();
        a.setX(5);
        a.setY(2);
        System.out.println("Point at (" + a.getX() + ", " + a.getY());
        a.translate(-1, -1);
        System.out.println("Point at (" + a.getX() + ", " + a.getY());
        a.resetToOrigin();
        System.out.println("Point at (" + a.getX() + ", " + a.getY());
        Point2D b = new Point2D();
        Point2D c = new Point2D();
        System.out.println(b.toString());
        System.out.println(c); //Question: why don’t I need c.toString() here?
        System.out.println("Are b and c equal:" + b.equals(c));
    }
}


