package com.company;

public class Picture {
    private Object[] myShapes = new Object[100];
    private int numElements = 0;

    /**
     * add
     * <p>
     * sets value n in array data at numElements element, add one to numElements
     */
    public void add(Object shape) {
        this.myShapes[this.numElements] = shape;
        this.numElements++;
    }

    /**
     * toString
     * <p>
     * returns retVal
     */
    public String toString() {
        String retVal = "";
        for (int i = 0; i < this.numElements; i++) {
            retVal += this.myShapes[i] + ", ";
        }
        return retVal;
    }

}
