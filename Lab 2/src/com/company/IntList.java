/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/15/2021
 * IntList class
 */
package com.company;

public class IntList {
    private int[] data = new int[100];
    private int numElements = 0;

    /**
     * add
     *
     * sets value n in array data at numElements element, add one to numElements
     */
    public void add(int n){
        this.data[this.numElements] = n;
        this.numElements++;
    }

    /**
     * toString
     *
     * returns retVal
     */
    public String toString(){
        String retVal = "";
        for (int i = 0; i < this.numElements; i++){
            retVal += this.data[i] + ", ";
        }
        return retVal;
    }

    /**
     * sum
     *
     * return sum of array data elements
     */
    public int sum(){
        int sum = 0;
        for(int i = 0; i < this.numElements; i++){
            sum += data[i];
        }
        return sum;
    }
    /**
     * indexOf
     *
     * return sum of array data elements
     */
    public int indexOf(int target){
        for(int i = 0; i < this.numElements; i++){
            if (data[i]==target){
                return i;
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        IntList a = new IntList();
        a.add(95); a.add(100); a.add(58);
        System.out.println(a.toString() );
        System.out.println(a.sum() );
        System.out.println(a.indexOf(95)); //uncomment these to work on next
        System.out.println(a.indexOf(20));
//        System.out.println(a.save() );
    }
}

