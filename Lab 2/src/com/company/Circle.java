/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/16/2021
 * Circle class
 */
package com.company;

public class Circle {
    private int x;
    private int y;
    private double radius;
    private String shape = "O";

    /**
     * default constructor
     */
    public Circle() {
        this.x = 1;
        this.y = 1;
        this.radius = 1;
    }

    /**
     * parameterized constructor
     * <p>
     * takes new x and new y
     */
    public Circle(int newX, int newY) {
        this.x = newX;
        this.y = newY;
        this.radius = 1;
    }

    /**
     * parameterized constructor
     * <p>
     * takes newX, newY, and newRadius
     */
    public Circle(int newX, int newY, double newRadius) {
        this.x = newX;
        this.y = newY;
        this.radius = newRadius;
    }

    /**
     * draw
     * <p>
     * prints shape
     */
    public void draw() {
        System.out.println(this.shape);
    }

    /**
     * getX
     * <p>
     * return x
     */
    public int getX() {
        return this.x;
    }

    /**
     * getY
     * <p>
     * return y
     */
    public int getY() {
        return this.y;
    }

    /**
     * getRadius
     * <p>
     * return radius
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * getArea
     * <p>
     * return PI * radius^2
     */
    public double getArea() {
        return Math.PI * (radius * radius);
    }

    /**
     * setX
     * <p>
     * set x to newX
     */
    public void setX(int newX) {
        this.x = newX;
    }

    /**
     * setY
     * <p>
     * set y to newY
     */
    public void setY(int newY) {
        this.y = newY;
    }

    /**
     * setRadius
     * <p>
     * set radius to nR
     */
    public void setRadius(double nR) {
        this.radius = nR;
    }

    /**
     * toString override
     * <p>
     * returns shape
     */
    @Override
    public String toString() {
        return this.shape;
    }

    /**
     * equals
     * <p>
     * returns true this x is equal to that x, this y equals that y and
     * this radius equals that radius
     */
    public boolean equals(Circle that) {
        return this.x == that.x && this.y == that.y && this.radius == that.radius;
    }
}
