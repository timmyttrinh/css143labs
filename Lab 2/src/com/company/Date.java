/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/15/2021
 * Data class
 */
package com.company;

public class Date {
    private int month;
    private int day;
    private int year;

    /**
     * Date
     * <p>
     * default constructor
     */
    public Date() {
        this.month = 1;
        this.day = 1;
        this.year = 2021;
    }

    /**
     * Date
     * <p>
     * parameterized constructor
     */
    public Date(int newMonth, int newDay, int newYear) {
        setMonth(newMonth);
        setDay(newDay);
        setYear(newYear);
    }

    /**
     * setDate
     * <p>
     * set date newMonth, newDay, newYear
     */
    public void setDate(int newMonth, int newDay, int newYear) {
        setMonth(newMonth);
        setDay(newDay);
        setYear(newYear);
    }

    /**
     * setMonth
     * <p>
     * set month to newMonth
     */
    private void setMonth(int newMonth) {
        if (newMonth < 1 || newMonth > 12) {
            System.out.println("invalid month, defaulted to 1");
            this.month = 1;
        } else {
            this.month = newMonth;
        }
    }

    /**
     * setDay
     * <p>
     * set day to newDay
     */
    private void setDay(int newDay) {
        if (newDay < 1 || newDay > 31) {
            System.out.println("invalid day, defaulted to 1");
            this.day = 1;
        } else {
            this.day = newDay;
        }
    }

    /**
     * setYear
     * <p>
     * set year to newYear
     */
    private void setYear(int newYear) {
        if (newYear < 0 || newYear > 2021) {
            System.out.println("invalid year, defaulted to 2021");
            this.year = 2021;
        } else {
            this.year = newYear;
        }
    }

    /**
     * report
     * <p>
     * prints the date
     */
    public void report() {
        System.out.println(this.month + "/" + this.day + "/" + this.year);
    }

}
