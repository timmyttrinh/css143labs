/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/16/2021
 * Square class
 */
package com.company;

public class Square {
    private int x;
    private int y;
    private double sideLength;
    private String shape = "[]";

    /**
     * default constructor
     *
     */
    public Square() {
        this.x = 1;
        this.y = 1;
        this.sideLength = 1;
    }

    /**
     * parameterized constructor
     *
     * takes new x and new y
     */
    public Square(int newX, int newY) {
        this.x = newX;
        this.y = newY;
        this.sideLength = 1;
    }

    /**
     * parameterized constructor
     *
     * takes newX, newY, and sideLength
     */
    public Square(int newX, int newY, double newSideLength) {
        this.x = newX;
        this.y = newY;
        this.sideLength = newSideLength;
    }

    /**
     * draw
     *
     * prints shape
     */
    public void draw() {
        System.out.println(this.shape);
    }

    /**
     * getX
     *
     * return x
     */
    public int getX() {
        return this.x;
    }

    /**
     * getY
     *
     * return y
     */
    public int getY() {
        return this.y;
    }

    /**
     * getSideLength
     *
     * return sideLength
     */
    public double getSideLength() {
        return this.sideLength;
    }

    /**
     * getArea
     *
     * return sideLength * sideLength
     */
    public double getArea() {
        return this.sideLength * this.sideLength;
    }

    /**
     * setX
     *
     * set x to newX
     */
    public void setX(int newX) {
        this.x = newX;
    }

    /**
     * setY
     *
     * set y to newY
     */
    public void setY(int newY) {
        this.y = newY;
    }

    /**
     * setSideLength
     *
     * set sideLength to sl
     */
    public void setSideLength(double sl) {
        this.sideLength = sl;
    }

    /**
     * toString override
     *
     * returns shape
     */
    @Override
    public String toString() {
        return this.shape;
    }

    /**
     * equals
     *
     * returns true this x is equal to that x, this y equals that y and
     * this sideLength equals that sideLength
     */
    public boolean equals(Square that) {
        return this.x == that.x && this.y == that.y && this.sideLength == that.sideLength;
    }
}
