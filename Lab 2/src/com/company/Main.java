package com.company;

public class Main {

    public static void main(String[] args) {
        Date d1 = new Date();
        d1.report();
        d1.setDate(7, 27, 1999);
        d1.report();
        Date d2 = new Date(-2,100,20111);
        d2.report();
    }
}
