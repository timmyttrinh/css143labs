/**
 * @author Timmy Trinh
 * @since 02/07/2021
 * SimpleColor class,
 */
package com.company;

public class SimpleColor {
    private int r;
    private int g;
    private int b;

    /**
     * r getter
     *
     * @pre: none
     * @post: returns r
     */
    public int getR() {
        return this.r;
    }

    /**
     * r setter
     *
     * @pre: none
     * @post: assigns r value r
     */
    public void setR(int r) {
        if (r < 0 || r > 255) {
           throw new ColorException();
        } else {
            this.r = r;
        }
    }

    /**
     * g getter
     *
     * @pre: none
     * @post: returns g
     */
    public int getG() {
        return g;
    }

    /**
     * g setter
     *
     * @pre: none
     * @post: assigns g value g
     */
    public void setG(int g) {
        if (g < 0 || g > 255) {
            throw new ColorException();
        } else {
            this.g = g;
        }
    }

    /**
     * b getter
     *
     * @pre: none
     * @post: returns b
     */
    public int getB() {
        return b;
    }

    /**
     * b setter
     *
     * @pre: none
     * @post: assigns b value b
     */
    public void setB(int b) {
        if (b < 0 || b > 255) {
            throw new ColorException();
        } else {
            this.b = b;
        }
    }

    /**
     * setColor method
     *
     * @pre: none
     * @post: uses setters to assign r, g, and b with values a, b and c
     */
    public void setColor(int a, int b, int c) {
        setR(a);
        setG(b);
        setB(c);
    }

    /**
     * default SimpleColor constructor
     *
     * @pre: none
     * @post: assigns r, g, and b default values of 0
     */
    public SimpleColor() {
        this.r = 0;
        this.g = 0;
        this.b = 0;
    }

    /**
     * SimpleColor constructor with parameters r, g, and b
     *
     * @pre: none
     * @post: uses setters to initialize a SimpleColor object
     * with values r, g, and b
     */
    public SimpleColor(int r, int g, int b) {
        setR(r);
        setG(g);
        setB(b);
    }

    /**
     * SimpleColor copy constructor
     *
     * @pre: SimpleColor object b cannot be null
     * @post: assigns r, g, and b with values from SimpleColor object b
     */
    public SimpleColor(SimpleColor b) {
        this.r = b.r;
        this.g = b.g;
        this.b = b.b;
    }

}
