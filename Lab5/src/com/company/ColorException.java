/**
 * @author Timmy Trinh
 * @since 02/07/2021
 * ColorException class, inherits from RuntimeException
 */
package com.company;

public class ColorException extends RuntimeException {

    /**
     * default ColorException constructor
     *
     * @pre: none
     * @post: calls super class with error message
     */
    public ColorException() {
        super("An error occurred in Color");
    }

    /**
     * ColorException constructor
     *
     * @pre: none
     * @post: calls super class using String msg
     */
    public ColorException(String msg) {
        super(msg);
    }

    //test
    public static void main(String[] args) {
        throw new ColorException("this is a test");
    }

}
