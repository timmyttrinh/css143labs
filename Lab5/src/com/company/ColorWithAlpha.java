/**
 * @author Timmy Trinh
 * @since 02/07/2021
 * ColorWithAlpha class, inherits from SimpleColor
 */
package com.company;

public class ColorWithAlpha extends SimpleColor {
    private int alpha;

    /**
     * ColorWithAlpha constructor, takes newAlpha as input
     *
     * @pre: newAlpha must be between 0-255
     * @post: assign alpha value newAlpha, and assign RGB values 0
     */
    public ColorWithAlpha(int newAlpha) {
        setAlpha(newAlpha);
        setColor(0, 0, 0);
    }

    /**
     * ColorWithAlpha constructor, takes r, g, b, and a as input
     *
     * @pre: r, g, b, and a must be between 0-255
     * @post: assign r, g, b , and a the passed values
     */
    public ColorWithAlpha(int r, int g, int b, int a) {
        super(r, g, b); //calls parent constructor
        setAlpha(a);
    }

    /**
     * ColorWithAlpha copy constructor
     *
     * @pre: other ColorWithAlpha object cannot be null
     * @post: assign r, g, b, and a values from other object
     */
    public ColorWithAlpha(ColorWithAlpha other) {
        if (other == null) {
            System.out.println("null object");
        } else {
            setColor(other.getR(), other.getG(), other.getB());
            setAlpha(other.getAlpha());
        }
    }

    /**
     * toString method Override
     *
     * @pre: none
     * @post: return string representation of rgb and a
     */
    @Override
    public String toString() {
        return "r:" + getR() + " g:" + getG() + " b:" + getB() + " a:" + getAlpha();
    }

    /**
     * equals method Override
     *
     * @pre: object o cannot be null
     * @post: returns true if ColorWithAlpha objects are equal
     */
    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        if(o == this){
            return true;
        }

        ColorWithAlpha other = (ColorWithAlpha) o;

        return this.getR() == other.getR() && this.getG() == other.getG() &&
                this.getB() == other.getB() && this.getAlpha() == other.getAlpha();
    }

    /**
     * alpha getter
     *
     * @pre: none
     * @post: return alpha
     */
    public int getAlpha() {
        return alpha;
    }

    /**
     * alpha setter
     *
     * @pre: none
     * @post: assign alpha value a
     */
    public void setAlpha(int a) {
        if (a < 0 || a > 255) {
            throw new ColorException();
        } else {
            this.alpha = a;
        }
    }
}
