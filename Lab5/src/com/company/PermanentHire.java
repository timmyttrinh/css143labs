/**
 * @author Timmy Trinh
 * @since 02/07/2021
 * PermanentHire class,
 */
package com.company;

public class PermanentHire extends SalariedWorker{

    public static final double MONTHLY_PAY = 10000;

    public static final double MONTHLY_BONUS = 500;
    /**
     * default constructor, cannot be used
     */
    private PermanentHire(){

    }

    /**
     * PermanentHire constructor, takes name and social as input
     *
     * @pre: none
     * @post: call super class using name, social, and MONTHLY_PAY
     */
    public PermanentHire(String name, int social){
        super(name, social, MONTHLY_PAY);
    }

    /**
     * PermanentHire constructor, takes name, social, and monthlyPay as input
     *
     * @pre: none
     * @post: call super class using name, social, and monthlyPay
     */
    public PermanentHire(String name, int social, double monthlyPay){
        super(name, social, monthlyPay);
    }

    /**
     * calculateWeeklyPay method Override
     *
     * @pre: none
     * @post: returns MonthlyPay + MONTHLY_BONUS divided by 4
     */
    @Override
    public double calculateWeeklyPay() {
        //implement getter for SalariedWorker monthly pay or use protected instance variable
        //return (super.monthlyPay + MONTHLY_BONUS) / 4;
        return (getMonthlyPay() + MONTHLY_BONUS) / 4;
    }


}
