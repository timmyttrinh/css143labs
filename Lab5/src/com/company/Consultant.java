/**
 * @author Timmy Trinh
 * @since 02/07/2021
 * Consultant class,
 */
/*
12. You cannot store ColorException objects in an ArrayList of Employees
You would need to declare the ArrayList as an ArrayList of Objects rather than Employees

The method that is polymorphic is calculatePayWeekly()

We could randomize an employees social number.

public class aClass(){
public getRandomEmployee(){does something}
}

public class bClass(){
@Override
public getRandomEmployee(){does something else}
}
 */
package com.company;

public class Consultant extends HourlyWorker{

    public static final double MINIMUM_WAGE = 3.0;

    /**
     * default constructor, cannot be used
     */
    private Consultant(){

    }

    /**
     * Consultant constructor, takes name, social
     *
     * @pre: none
     * @post: call super using name, social, and MINIMUM_WAGE
     */
    public Consultant(String name, int social){
        super(name, social, MINIMUM_WAGE);
    }

    /**
     * Consultant constructor, takes name, social, hourlyPay
     *
     * @pre: none
     * @post: calls super using name, social, and hourlyPay
     */
    public Consultant(String name, int social, double hourlyPay){
        super(name, social, hourlyPay);
    }

    /**
     * calculateWeeklyPay method Override
     *
     * @pre: none
     * @post: returns HourlyPay() multiplied by 20 hours
     */
    @Override
    public double calculateWeeklyPay() {
        //implement getter for HourlyWorker monthly pay or use protected instance variable
        //return super.hourlyPay * 20;
        return getHourlyPay() * 20;
    }
}
