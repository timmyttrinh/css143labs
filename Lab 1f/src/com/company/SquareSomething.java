package com.company;

public class SquareSomething {

    public static int square(int num){
        return num*num;
    }
    public static double square(double num){
        return num*num;
    }
    public static float square(float num){
        return num*num;
    }
}
