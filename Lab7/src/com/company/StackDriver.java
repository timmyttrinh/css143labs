/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/22/2021
 * StackDriver, linked list stack tests, tests borrowed from lab 7
 */
package com.company;

public class StackDriver {

    public static void main(String[] args) {
        LLStack a = new LLStack();
        // Queue q = new LinkedList();
        // For queue driver, change the a.push() calls to q.add() or q.offer()
        // and change the a.pop to q.poll or q.remove
        a.push('R');
        a.push('a');
        a.push('c');
        a.push('e');
        a.push('c');
        a.push('a');
        a.push('r');
        System.out.println("Size : " + a.size());
        System.out.println(a.toString());
        System.out.println(a.pop());
        System.out.println(a.pop());
        while(!a.isEmpty()) {
            System.out.println(a.pop());
        }


    }
}
