/**
 * @author Timmy Trinh borrowed from Meng Yang
 * @version 1.0.0
 * @since 02/22/2021
 * Linked List Queue
 */
package com.company;

/*
 * This class implements a queue with linked list
 * Author: Meng Yang
 * Date: Fall 2018
 */

public class LLQueue<T> {
    // This is an inner class specifically utilized for LLStack class,
    // thus no setter or getters are needed
    private class Node {
        private Object data;
        private Node next;

        // Constructor with no parameters for inner class
        public Node() {
            this.data = null;
            this.next = null;
        }

        // Parametrized constructor for inner class
        public Node(Object newData, Node nextLink) {
            // to do: Data part of Node is an Object
            // to do: Link to next node is a type Node
            this.data = newData;
            this.next = nextLink;
        }
        @Override
        public String toString(){
            return this.data + "";
        }
    }

    private Node front;
    private Node back;

    /**
     * default constructor
     *
     * @pre none
     * @post assign null to front and back
     */
    public LLQueue() {
        // to do
        this.front = null;
        this.back = null;
    }

    //offer(enqueue) adds the object at the back of the queue
    public void enqueue(Object o) {
        // to do
        Node temp = new Node(o, null);

        if (this.back == null) {
            this.front = temp;
            this.back = temp;
            return;
        }
        this.back.next = temp;
        this.back = temp;
    }

    //poll(dequeue): retrieves and removes the head of this queue,
    //or returns null if this queue is empty.
    public Object dequeue() {
        // to do
        Object dequeued = this.front;
        if(this.front == null){
            return null;
        }
        Node temp = this.front;
        this.front = this.front.next;

        if(this.front == null){
            this.back = null;
        }
        return dequeued;
    }

    // Returns the size of linked list by traversing the list
    public int size() {
        // to do
        int count = 0;
        Node cur = this.front;
        while (cur != null) {
            cur = cur.next;
            count++;
        }
        return count;
    }

    //peek: Retrieves, but does not remove, the head of this queue,
    //or returns null if this queue is empty.
    public Object peek() {
        // to do
        if(this.front == null){
            return null;
        }else {
            return this.front;
        }
    }

    /**
     * isEmpty method
     *
     * @pre none
     * @post return true if the size of the list is 0
     */
    public boolean isEmpty() {
        // to do
        return size() == 0;
    }

    // For two lists to be equal they must contain the same data items in
    // the same order. The equals method of T is used to compare data items.
    public boolean equals(Object otherObject) {
        if (otherObject == null)
            return false;

        else if (!(otherObject instanceof LLQueue)) {
            return false;
        } else {
            LLQueue otherList = (LLQueue) otherObject;
            if (size() != otherList.size())
                return false;
            Node position = front;
            Node otherPosition = otherList.front;
            while (position != null) {
                if (!(position.data.equals(otherPosition.data)))
                    return false;
                position = position.next;
                otherPosition = otherPosition.next;
            }
            return true; // objects are the same
        }

    }

    /**
     * toString method
     *
     * @pre none
     * @post uses while loop to traverse the nodes, creating string of the data
     */
    public String toString() {
        String retValue = "";
        Node current = this.front;

        while (current != null) {
            retValue += current.data.toString() + " ";
            current = current.next;
        }
        return retValue;
    }
    // There is no need to modify the driver
    public static void main(String[] args) {
        // input data for testing
        String target = "Somethings!";
        String palindrome = "a man a plan canal panama";

        LLQueue list = new LLQueue();
        // objects to be added to list
        Object object1 = (Character) target.charAt(4);
        Object object2 = (Character) target.charAt(1);
        Object object3 = (Character) target.charAt(2);
        Object object4 = (Character) target.charAt(9);
        Object object20 = (Character) target.charAt(6); // will not be added to list

        // add 4 objects to our linked list
        list.enqueue(object1);
        list.enqueue(object2);
        list.enqueue(object3);
        list.enqueue(object4);

        // make sure all are added
        System.out.println("My list has " + list.size() + " nodes.");

        //testing equals
        LLQueue list2 = new LLQueue();
        // add 4 objects to the new linked list
        list2.enqueue(object1);//t
        list2.enqueue(object2);//o
        list2.enqueue(object3);//m
        list2.enqueue(object4);//s
        boolean isEqual2 = list.equals(list2);
        System.out.println("list2 is equal to list1? " + isEqual2);

        // add 4 objects to our linked list in a different order
        LLQueue list3 = new LLQueue();
        list3.enqueue(object3);//m
        list3.enqueue(object1);//t
        list3.enqueue(object2);//o
        list3.enqueue(object4);//s
        boolean isEqual3 = list.equals(list3);
        System.out.println("list3 is equal to list1? " + isEqual3);

        // testing isEmpty() and poll()
        while (!list.isEmpty()) {
            Object temp = list.dequeue();
            System.out.println("Polling " + temp);
        }

    }


}

