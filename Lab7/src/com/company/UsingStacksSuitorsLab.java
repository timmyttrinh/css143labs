/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/22/2021
 * UsingStacksSuitorLab
 */
package com.company;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/* CSSSKL 162
 *
 * UsingStacksSuitorsLab
 *
 * This class is mostly a driver for playing with Strings as palindromes,
 * both iteratively and recursively.  The UsingStacksSuitorsLab class itself is
 * a runnable object, so it can be passed to a thread in our Queue demo
 *
 *
 */

public class UsingStacksSuitorsLab implements Runnable {
    private static int threadCount = 0;
    private String name;

    public UsingStacksSuitorsLab() {
        name = "#" + threadCount++ + "Thread";
    }

    public static void main(String[] args) {
        String s1 = "food";            //not a palindrome
        String s2 = "racecar";      //a palindrome

        System.out.println("String1 is \"" + s1 + "\"");
        System.out.println("String2 is \"" + s2 + "\"");

        System.out.println(s1 + " reversed is: ");
        printReverse(s1);
        System.out.println(s2 + " reversed is: ");
        printReverse(s2);

        System.out.println();
        recPrintReverse(s1);
        System.out.println();
        recPrintReverse(s2);
        System.out.println();

        System.out.println(s1 + " is a palindrome: " + isPalindrome(s1));
        System.out.println(s2 + " is a palindrome: " + isPalindrome(s2));

        System.out.println(s1 + " is a palindrome(recursively): " + isPalindromeRec(s1));
        System.out.println(s2 + " is a palindrome(recursively): " + isPalindromeRec(s2));

        System.out.println("Did we build a Queue of Threads and start them? " + buildThreadQueue());

        int n = 6;
        System.out.println("For " + n + " suitors, stand in place:" + findPlaceToStand(n));

        n = 10;
        System.out.println("For " + n + " suitors, stand in place:" + findPlaceToStand(n));

        n = 6;
        System.out.println("For " + n + " suitors, stand in place:" + reverseFindPlaceToStand(n));

        n = 10;
        System.out.println("For " + n + " suitors, stand in place:" + reverseFindPlaceToStand(n));
    }

    /**
     * printReverse method
     *
     * @pre none
     * @post prints the target string backwards
     */
    public static void printReverse(String target) {
        LLStack str = new LLStack();
        String print = "";
        for (int i = 0; i < target.length(); i++) {
            str.push(target.charAt(i));
        }
        while (!str.isEmpty()) {
            print += str.pop();
        }
        System.out.println(print);
    }

    /**
     * recPrintReverse
     *
     * @pre none
     * @post recursively prints target String backwards
     */
    public static void recPrintReverse(String target) {
        if (target.isEmpty() || target.length() < 2) {
            System.out.println(target);
        } else {
            System.out.print(target.charAt(target.length() - 1));
            recPrintReverse(target.substring(0, target.length() - 1));
        }
    }

    /**
     * isPalindrome method
     *
     * @pre none
     * @post return true if the string created with a stack is equal to the input string
     */
    public static boolean isPalindrome(String input) {
        //todo: use a stack
        LLStack str = new LLStack();
        String reverse = "";
        for (int i = 0; i < input.length(); i++) {
            str.push(input.charAt(i));
        }
        while (!str.isEmpty()) {
            reverse += str.pop();
        }
        return input.equals(reverse);
    }

    /**
     * isPalindromeRec method
     *
     * @pre none
     * @post uses recursion to compare the first and last letters until reaching
     * the half, return true if equal
     */
    public static boolean isPalindromeRec(String sentence) {
        //todo
        if (sentence.length() < 2) {
            return true;
        } else if (sentence.charAt(0) != sentence.charAt(sentence.length() - 1)) {
            return false;
        }
        return isPalindrome(sentence.substring(1, sentence.length() - 1));

    }

    /**
     * findPLaceToStand
     *
     * @pre none
     * @post return the suitor position that would be the last standing
     *
     */
    public static int findPlaceToStand(int numSuitors) {
        //todo
        Queue<Integer> q = new LinkedList<Integer>();
        for (int i = 1; i <= numSuitors; i++) {
            q.add(i);
        }
        while (q.size() > 1) {
            if (q.size() > 2) {
                q.add(q.remove());
                q.add(q.remove());
                q.remove();
            } else {
                q.remove();
            }
        }
        return q.remove();
    }

    /**
     * reverseFindPLaceToStand
     *
     * @pre none
     * @post find suitor position that would be last standing
     *
     */
    public static int reverseFindPlaceToStand(int numSuitors) {
        Stack<Integer> s = new Stack<Integer>();
        Stack<Integer> s2 = new Stack<Integer>();
        int retVal = 0;
        for (int i = 1; i <= numSuitors; i++) {
            s.push(i);
        }
        // put it back in standard order
        while (!s.isEmpty()) {
            s2.push(s.pop());
        }
        while (s2.size() > 1) {
            if (s2.size() > 2) {
                s2.insertElementAt(s2.pop(), 0);
                s2.insertElementAt(s2.pop(), 0);
                s2.pop();
            } else {
                s2.pop();
            }
        }

        return s2.pop();
    }


    public static boolean buildThreadQueue() {    //returns true upon success
        Queue<Thread> q = new LinkedList<Thread>();

        //when our program starts up, it might create multiple threads to use
//        q.enqueue( new Thread(new UsingStacksSuitorsLab()));
//        q.enqueue( new Thread(new UsingStacksSuitorsLab()));
//        q.enqueue( new Thread(new UsingStacksSuitorsLab()));

        System.out.println("Initial Thread order:");
        q.toString();

        //We need to iterate over our pool of threads and call start() on each one
        //Make a loop that dequeues a thread, calls start on it, and //enqueues it again
        //to do:
        //current = get a thread
        //current.start();
        //put the thread back

        System.out.println("Thread order after start()ing:");
        q.toString();

        return true;  //on successful start
    }


    /*
     * No need to edit anything below here,
     * unless you'd like to change the
     * behaviour of each thread in the thread pool above
     */

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(name + ": " + i + "th iteration");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                //do nothing here
            }
        }
    }
}
