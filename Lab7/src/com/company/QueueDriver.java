/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 02/22/2021
 * Queue Driver, linked list queue tests, tests borrowed from Lab 7
 */
package com.company;

public class QueueDriver {

    public static void main(String[] args) {
        LLQueue a = new LLQueue();
        // Queue q = new LinkedList();
        // For queue driver, change the a.push() calls to q.add() or q.offer()
        // and change the a.pop to q.poll or q.remove
        a.enqueue('R');
        a.enqueue('a');
        a.enqueue('c');
        a.enqueue('e');
        a.enqueue('c');
        a.enqueue('a');
        a.enqueue('r');
        System.out.println("Size : " + a.size());
        System.out.println(a.toString());
        while (!a.isEmpty()) {
            System.out.println(a.dequeue());
        }
    }
}
