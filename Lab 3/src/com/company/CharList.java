/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/24/2021
 * CharList class
 */
package com.company;

public class CharList {
    private char[] charList = new char[1];
    private int charNum = 0;

    /**
     * default constructor
     */
    public CharList() {

    }

    /**
     * parameterized constructor
     * <p>
     * takes String parameter
     */
    public CharList(String startStr) {
        char[] temp = new char[startStr.length()];
        this.charNum = startStr.length();
        for (int i = 0; i < startStr.length(); i++) {
            temp[i] = startStr.charAt(i);
        }
        this.charList = temp;
    }

    /**
     * copy constructor
     */
    public CharList(CharList other) {
        if (other == null) {
            System.out.println("null CharList");
        } else {
            setCharList(other.charList);
            setCharNum(other.charNum);
        }
    }

    /**
     * add
     *
     * adds char next at the end of the array, increase array size by one
     */
    public void add(char next) {
        this.charNum++;
        char[] temp = new char[this.charNum];
        for (int i = 0; i < this.charList.length; i++) {
            temp[i] = this.charList[i];
        }
        temp[this.charNum - 1] = next;
        this.charList = temp;

    }

    /**
     * get
     * <p>
     * returns char at index
     */
    public char get(int index) {
        return this.charList[index];
    }

    /**
     * toString Override
     * <p>
     * return String representation of char array
     */
    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < this.charList.length; i++) {
            result += this.charList[i];
        }
        return result;
    }

    /**
     * equals
     * <p>
     * return true if this CharList equals other CharList
     */
    public boolean equals(Object otherList) {
        boolean retBool = true;
        if (otherList == null) {
            return false;
        }
        if (otherList == this) {
            return true;
        }
        CharList that = (CharList) otherList;
        if (this.charList.length != that.charList.length) {
            return false;
        } else {
            for (int i = 0; i < this.charList.length; i++) {
                if (this.charList[i] != that.charList[i]) {
                    retBool = false;
                }
            }
        }
        return retBool;
    }

    public void setCharList(char[] charList) {
        this.charList = charList;
    }

    public void setCharNum(int charNum) {
        this.charNum = charNum;
    }

    public int size() {
        return this.charList.length;
    }
}
