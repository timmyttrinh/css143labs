/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/25/2021
 * Math2 class
 */
package com.company;

/**
 * max
 * <p>
 * return larger int
 */
public class Math2 {
    public static final double PI = Math.PI;
    public static final double E = 2.718;

    public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    /**
     * max
     * <p>
     * return larger double
     */
    public static double max(double a, double b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    /**
     * pow
     * <p>
     * return base to the power of exp
     */
    public static int pow(int base, int exp) {
        int pow = 1;
        for (int i = 0; i < exp; i++) {
            pow *= base;
        }
        return pow;
    }
}
