/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/25/2021
 * Fraction class
 */
package com.company;

public class Fraction {
    public final int numerator;
    public final int denominator;

    public Fraction(int num, int den) {
        if (num < 0) {
            num = num - (num * 2);
            den = den - (den * 2);
        }
        if (den == 0) {
            System.out.println("denominator cannot be 0, defaulted to 1");
            this.numerator = 1;
            this.denominator = 1;
        } else {
            this.numerator = (num / gcd(num, den));
            this.denominator = (den / gcd(num, den));
        }
    }

    public Fraction(Fraction other) {
        this.numerator = other.numerator;
        this.denominator = other.denominator;
    }

    public Fraction add(Fraction that) {
        int num = (this.numerator * that.denominator) + (that.numerator * this.denominator);
        int den = this.denominator * that.denominator;
        Fraction f = new Fraction(num, den);
        return f;
    }

    /**
     * gcd
     * <p>
     * calculates greatest common divisor of the numerator and the denominator using the Euclidean algorithm
     *
     * @PRE: none
     * @POST: return greatest common divisor
     */
    private static int gcd(int numGcd, int denGcd) {
        numGcd = Math.abs(numGcd);
        denGcd = Math.abs(denGcd);
        if (numGcd == 0) {
            return denGcd;
        }
        while (denGcd != 0) {
            if (numGcd > denGcd) {
                numGcd = numGcd - denGcd;
            } else {
                denGcd = denGcd - numGcd;
            }
        }
        return numGcd;
    }

    /**
     * equals method override
     * <p>
     * compares two objects
     *
     * @PRE: none
     * @POST: return true if this object equals other object
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        Fraction o = (Fraction) other;
        if (this.numerator == o.numerator &&
                this.denominator == o.denominator) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * toString Override
     * <p>
     * string of fraction
     *
     * @PRE: none
     * @POST: return string representation of the fraction
     */
    @Override
    public String toString() {
        return this.numerator + "/" + this.denominator;
    }

    /**
     * getNumerator
     * <p>
     * returns numerator
     */
    public int getNum() {
        return numerator;
    }

    /**
     * getDenominator
     * <p>
     * returns denominator
     */
    public int getDenom() {
        return denominator;
    }

//    /**
//     * setNumerator
//     * <p>
//     * numerator set to value newNum
//     */
//    public void setNum(int newNum) {
//        this.numerator = newNum;
//    }
//
//    /**
//     * setDenominator
//     * <p>
//     * denominator set to value newDen
//     */
//    public void setDenom(int newDen) {
//        if (newDen == 0) {
//            System.out.println("cannot set denominator to 0, fraction defaulted to 1/1");
//            this.denominator = 1;
//        } else {
//            this.denominator = newDen;
//        }
//    }
}
