/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/25/2021
 * LineSegment class
 */
/*
What is a privacy leak?
    A privacy leak is when an instance variable is able to be modified outside of its class.
Do your getters or setters have privacy leaks?
    Yes they have privacy leaks.
Where else could a privacy leak occur?
    Privacy leaks can occur anywhere you are not using a copy constructor.
 */

package com.company;

public class LineSegment {
    private Point2D startPoint = new Point2D();
    private Point2D endPoint = new Point2D();

    /**
     * default constructor
     */
    public LineSegment() {
        this.startPoint.setX(0);
        this.startPoint.setY(0);

        this.endPoint.setX(0);
        this.endPoint.setY(0);
    }

    /**
     * LineSegment constructor
     *
     * takes Point2D objects as parameters
     */
    public LineSegment(Point2D start, Point2D end) {
        this.startPoint.setX(start.getX());
        this.startPoint.setY(start.getY());
        this.endPoint.setX(end.getX());
        this.endPoint.setY(end.getY());
    }

    /**
     * copy constructor
     *
     * takes another LineSegment object as a parameter
     */
    public LineSegment(LineSegment other){
        this.startPoint.setX(other.getStartPoint().getX());
        this.startPoint.setY(other.getStartPoint().getY());
        this.endPoint.setX(other.getEndPoint().getX());
        this.endPoint.setY(other.getEndPoint().getY());
    }

    /**
     * distance
     *
     * returns the distance between two points
     */
    public double distance(){
        double x = Math.abs(this.startPoint.getX() - this.endPoint.getX()) *
                Math.abs(this.startPoint.getX() - this.endPoint.getX());
        double y = Math.abs(this.startPoint.getY() - this.endPoint.getY()) *
                Math.abs(this.startPoint.getY() - this.endPoint.getY());
        double d = x + y;
        return Math.sqrt(d);
    }
    /**
     * startPoint getter
     */
    public Point2D getStartPoint() {
        return startPoint;
    }

    /**
     * startPoint setter
     */
    public void setStartPoint(Point2D startPoint) {
        this.startPoint = startPoint;
    }

    /**
     * endPoint getter
     */
    public Point2D getEndPoint() {
        return endPoint;
    }

    /**
     * endPoint setter
     */
    public void setEndPoint(Point2D endPoint) {
        this.endPoint = endPoint;
    }

    /**
     * toString Override
     * <p>
     * returns string representation of a line
     */
    @Override
    public String toString() {
        return "Line: start(" + this.startPoint.getX() + "," + this.startPoint.getY() +
                ") and end(" + this.endPoint.getX() + "," + this.endPoint.getY() + ")";
    }

    /**
     * equals
     * <p>
     * returns true if LineSegment object equals other LineSegment object
     */
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        LineSegment other = (LineSegment) o;
        return this.startPoint.getX() == other.startPoint.getX() && this.startPoint.getY() ==
                other.startPoint.getY() && this.endPoint.getX() == other.endPoint.getX() &&
                this.endPoint.getY() == other.endPoint.getY();
    }
}

