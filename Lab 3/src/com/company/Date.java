/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/24/2021
 * Date class
 */
package com.company;

public class Date {
    private int month;
    private int day;
    private int year;

    /**
     * default constructor
     */
    public Date() {
        this.month = 0;
        this.day = 0;
        this.year = 0;
    }

    /**
     * parameterized date constructor
     * <p>
     * takes in parameters m, d, and y
     */
    public Date(int m, int d, int y) {
        setMonth(m);
        setDay(d);
        setYear(y);
    }

    /**
     * copy constructor
     * <p>
     * uses setters to create a copy
     */
    public Date(Date other) {
        setMonth(other.month);
        setDay(other.day);
        setYear(other.year);
    }

    /**
     * setDate
     * <p>
     * sets date using three parameters
     */
    public void setDate(int m, int d, int y) {
        setMonth(m);
        setDay(d);
        setYear(y);
    }

    /**
     * toString Override
     * <p>
     * return String representation of the date
     */
    @Override
    public String toString() {
        return this.month + "." + this.day + "." + this.year;
    }

    /**
     * equals
     * <p>
     * returns true if date object equals other date object
     */
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        Date o = (Date) other;
        return this.month == o.month && this.day ==
                o.day && this.year == o.year;
    }

    /**
     * month getter
     */
    public int getMonth() {
        return month;
    }

    /**
     * month setter
     * <p>
     * checks if month is a value between 1-12
     */
    public void setMonth(int month) {
        if (month < 1 || month > 12) {
            System.out.println("invalid month,defaulted to 1");
            this.month = 1;
        } else {
            this.month = month;
        }
    }

    /**
     * day getter
     */
    public int getDay() {
        return day;
    }

    /**
     * day setter
     * <p>
     * checks if day is a value between 1-31
     */
    public void setDay(int day) {
        if (day < 1 || day > 31) {
            System.out.println("invalid day,defaulted to 1");
            this.day = 1;
        } else {
            this.day = day;
        }
    }

    /**
     * year getter
     */
    public int getYear() {
        return year;
    }

    /**
     * year setter
     * <p>
     * checks if year is positive
     */
    public void setYear(int year) {
        if (year < 0) {
            System.out.println("invalid year,defaulted to 2020");
            this.month = 2020;
        } else {
            this.year = year;
        }
    }
}
