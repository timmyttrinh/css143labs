/**
 * @author Timmy Trinh
 * @version 1.0.0
 * @since 01/24/2021
 * Shape class
 */
package com.company;

import java.awt.Color;

public class Shape {
    private int x;
    private int y;
    private Color color;

    /**
     * default constructor
     */
    public Shape() {
        this.x = 0;
        this.y = 0;
        this.color = null;
    }

    /**
     * parameterized constructor
     */
    public Shape(int x, int y, Color col) {
        this.x = x;
        this.y = y;
        this.color = col;
    }

    /**
     * copy constructor
     */
    public Shape(Shape other) {
        this.x = other.x;
        this.y = other.y;
        this.color = other.color;
    }

    /**
     * toString Override
     * <p>
     * return x, y, and color
     */
    @Override
    public String toString() {
        return "Shape{" +
                "x=" + x +
                ", y=" + y +
                ", color=" + color +
                '}';
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
        public double getArea(){
        return -1;
    }
//    public void draw(Graphics g){
//
//    }
}
