package com.company;

public class Car {
    private int odometer;
    private String make;
    private String model;

    /**
     * Car
     * <p>
     * default constructor
     */
    public Car() {
        this.odometer = 0;
        this.make = "Toyota";
        this.model = "AE86";
        //System.out.println(this); "this" references the default constructor,
        // so printing would be the same as printing a car object
    }

    public Car(String onlyMake){
        this.make = onlyMake;
    }

    public Car(String bothMake, String bothModel){
        this.make = bothMake;
        this.model = bothModel;
    }
    /**
     * Car
     *
     * param constructor
     */
    public Car(int carOdometer, String carMake, String carModel) {
        this.odometer = carOdometer;
        this.make = carMake;
        this.model = carModel;
    }

    /**
     * toString
     *
     * prints the odometer, make, and model of the object
     */
    @Override
    public String toString() {
return "Odometer: "+ this.odometer + ", Make: " + this.make +", Model: " + this.model;
    }

    public static void main(String[] args) {
        Car c1 = new Car();
        Car c2 = new Car(1000, "Toyota", "Supra");
        Car c3 = new Car("Toyota");
        Car c4 = new Car("Toyota", "Camry");

        System.out.println(c1.toString());
        System.out.println(c2.toString());
        System.out.println(c3.toString());
        System.out.println(c4.toString());
       // System.out.println(c1);
    }
}
