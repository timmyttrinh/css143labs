package com.company;

public class Main {

    public static void main(String[] args) {
        Point p = new Point();

        p.pub();
        //p.pri(); // "pri()" has private access in point

        System.out.println(p.y);
        //System.out.println(p.x); // 'x' has private access in point
    }
}
