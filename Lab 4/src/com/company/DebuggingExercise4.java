/**
 * @author Timmy Trinh
 * @version 02/03/2021
 */
/*
a1. The variable watch window allows you to monitor variables.
b1. The method call stack tracks method calls.
a3. Step over, moves to the next line.
b3. Step into, moves into the function.
c3. Step out, executes the method, moves line to after the method call.
d3. Continue, continues execution after breakpoint, exception, step
 */
package com.company;

class DebuggingExercise4
{
    public static void main(String[] args)
    {
        Account a = new Account("a");
        a.deposit(100);
        System.out.println(a.getOwner() + " has $" + a.getBalance());
        a.withdraw(200);
        System.out.println("After trying to withdraw $200, " + a.getOwner() + " has $" + a.getBalance());
    }
}
